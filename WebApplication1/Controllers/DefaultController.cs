﻿using reportManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class DefaultController : ApiController
    {
        [Route("api/oracle")]
        public IHttpActionResult getOracleReport()
        {
            var path = ReportManager.Instance.generateOracleReport();
            return Ok(path);
        }

        [Route("api/mssql")]
        public IHttpActionResult getMSSQLReport()
        {
            var path = ReportManager.Instance.generateMSSQLReport();
            return Ok(path);
        }
    }
}
