﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace reportManager
{
    public class ReportManager
    {
        private static ReportManager instance;

        private ReportManager() { }

        public static ReportManager Instance
        {
            get
            {
                if (instance == null) { instance = new ReportManager(); }
                return instance;
            }
        }

        public string generateOracleReport()
        {
            OracleReport report = new OracleReport();
            report.SetDatabaseLogon("user", "password");

            string filePath = "C:\\Temp\\oracle_report.pdf";
            report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filePath);

            return filePath;
        }

        public string generateMSSQLReport()
        {
            MSSQLReport report = new MSSQLReport();
            report.SetDatabaseLogon("user", "password");

            string filePath = "C:\\Temp\\mssql_report.pdf";
            report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filePath);

            return filePath;
        }
    }
}