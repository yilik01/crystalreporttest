﻿using reportManager;
using System;

namespace Standalone
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            ReportManager.Instance.generateOracleReport();
            ReportManager.Instance.generateMSSQLReport();
        }
    }
}
